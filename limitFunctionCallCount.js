function limitFunctionCallCount(cb, n) {
 if (n==undefined|| cb == undefined) {
    return "()";
 }
    let count = 0;

    return function () {

        if (count < n) {
            count++;
            return cb();
        } else {

            return null;
        }
    }
}
module.exports = limitFunctionCallCount;