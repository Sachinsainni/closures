function cacheFunction(cb) {

    let cache = {};
    const result = (parameter) => {
        if (parameter in cache) {
            console.log("Already cached!");
            return cache;
        }

        else {
            console.log("Add to the cache:");
            cache[parameter] = cb(parameter);
        }
        return cache[parameter];
    }
    return result;
}

module.exports = cacheFunction;