const counterFactory = () => {

    let counter = 0;

    return {
        increment() {
            counter++;
            return counter ;
        },
        decrement(value) {
            counter--;
           return counter;
        }
    }
}
module.exports = counterFactory;