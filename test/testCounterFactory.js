const counterfactory = require('../counterFactory');

const result = counterfactory();

console.log(result.increment());
console.log(result.decrement());