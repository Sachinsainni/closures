const limitFunctionCallCount = require('../limitFunctionCallCount');

const helloCallback = () => "hello";
const num = 3;

const result = limitFunctionCallCount(helloCallback, num);

for (let index = 0; index < 5; index++) {
    console.log(result())

}
